#!/usr/bin/python

import sys
from lexer_ import lexer
from parser_ import parser
from interpreter_ import interpreter

print('######################################################################')
print('#### WELCOME TO START LANG: v1.0 - Sep 07, 2019')
print('######################################################################')
while True:
    text = input('start >>> ')
    # Generate tokens
    tokens, error = lexer.run("<sdtin>", text)
    
    # if error from lexer, break
    if error:  
        print(error)
        continue
    # else: print(tokens)

    # Generate AST
    if not error: 
        ast, error = parser.run(tokens)

    # if error from perser, break
    if error: 
        print(error)
        continue
    # else: print(ast)


    # run the program
    if not error:
        result, error = interpreter.run(ast)
        
    if error: print(error)
    else: print(result)
    
    

