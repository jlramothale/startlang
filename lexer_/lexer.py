
"""
    LEXER
"""

from lexer_.token import ( 
    Token, DELIMETERS, ARIMETIC_OPERATORS, LITERALS, DIGITS, TT_EOF,
    KEYWORDS
)
from lib_.utils import CharUtils
from lib_.error import IllegalCharError
from lib_.position import Position

class Lexer:

    def __init__(self, fname, text):
        self.fname = fname
        self.text = text
        self.text_size = len(text)
        self.position = Position(-1, 0, -1, fname, text) # current position
        self.current_char = "" # current char
        # Get the first char
        self.nextChar()

    # advance to the next character from the source
    def nextChar(self):
        self.position.next(self.current_char)
        if self.position.index < self.text_size: self.current_char = self.text[self.position.index]
        else: self.current_char = None

    # tokenize the source into Tokens array/list
    def tokenize(self):
        tokens = []
        char_utils = CharUtils()
        while self.current_char != None:
            if char_utils.is_whitespace_or_newline(self.current_char): 
                self.nextChar()
            elif char_utils.is_arithmatic_operator(self.current_char):
                tokens.append(Token(char_utils.get_arithmatic_type(self.current_char), self.current_char, position_start=self.position))
                self.nextChar()
            elif char_utils.is_delimeter(self.current_char):
                tokens.append(Token(char_utils.get_delimeter_type(self.current_char), self.current_char, position_start=self.position))
                self.nextChar()
            elif char_utils.is_digit(self.current_char):
                tokens.append(self.make_number())
            elif char_utils.is_letter(self.current_char):
                tokens.append(self.make_identifier())
            else:
                position_start = self.position.copy()
                char = self.current_char
                self.nextChar()
                return [], IllegalCharError("'{}'".format(char), position_start, self.position) # returns empty list for the token
        # before returning tokens, add End of File token
        tokens.append(Token(TT_EOF, TT_EOF, position_start=self.position))
        return tokens, None # returns none for the error

    # creates number
    def make_number(self):
        number_str = ""
        dot_count = 0
        position_start = self.position.copy()
        char_utils = CharUtils()
        while self.current_char != None and char_utils.is_digit(self.current_char) or char_utils.is_dot(self.current_char):
            if char_utils.is_dot(self.current_char):
                # if already a dot, breaks, since a number has one dot
                if dot_count == 1: break 
                dot_count += 1
                number_str += self.current_char
            else: number_str += self.current_char
            self.nextChar()
        
        # is number is int, i.e. dot is zero
        if dot_count == 0: 
            return Token(char_utils.get_integer_type(), int(number_str), position_start, self.position)
        else:
            return Token(char_utils.get_float_type(), float(number_str), position_start, self.position)
    
    # creates either an identifier or keyword
    def make_identifier(self):
        identifier_str = ""
        position_start = self.position.copy()
        char_utils = CharUtils()
        # while current char is identifier char
        while self.current_char != None and char_utils.is_identifier(self.current_char):
            identifier_str += self.current_char
            self.nextChar()
        
        if char_utils.is_keyword(identifier_str):
            return Token(char_utils.get_keyword_type(), identifier_str, position_start, self.position)
        else:
            return Token(char_utils.get_identifier_type(), identifier_str, position_start, self.position)


# runs the lexer
def run(fname, text):
    lexer = Lexer(fname, text)
    tokens, errors = lexer.tokenize()

    return tokens, errors