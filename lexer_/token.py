
"""
    TOKEN
"""

class Token:

    def __init__(self, type, value, position_start=None, position_end=None):
        self.type = type
        self.value = value

        if position_start:
            self.position_start = position_start.copy()
            self.position_end = position_start.copy()
            self.position_end.next()

        if position_end:
            self.position_end = position_end

    def __repr__(self):
        return "{}: {}".format(self.type, self.value)

"""
    TOKEN TYPES
"""
import string

DIGITS = "012345678"
LETTERS = string.ascii_letters
LETTERS_DIGITS = DIGITS + LETTERS
INDETIFIER_CHARS = LETTERS_DIGITS + "_"

TT_PLUS = "PLUS"
TT_MINUS = "MINUS"
TT_DIV = "DIV"
TT_MUL = "MUL"
ARIMETIC_OPERATORS = {
    "DIV": "/",
    "MINUS": '-',
    "PLUS": '+',
    "MUL": '*',
}

TT_INT = "INT"
TT_FLOAT = "FLOAT"
TT_KEYWORD = "KEYWORD"
TT_IDENTIFIER = "IDENTIFIER"
LITERALS = {
    "FLOAT": "FLOAT",
    "INTEGER": "INT",
    "KEYWORD": "KEYWORD",
    "IDENTIFIER": "IDENTIFIER"
}

TT_LEFT_PAREN = "LEFT_PAREN"
TT_RIGHT_PAREN = "RIGHT_PARAN"
TT_EQUAL = "EQUAL"
TT_COLON = "COLON"
DELIMETERS = {
    "DOT": '.',
    "COLON": ':',
    "COMMA": ',',
    "LEFT_BRACE": '{',
    "LEFT_BRACKET": '[',
    "LEFT_PAREN": '(',
    "RIGHT_BRACE": '}',
    "RIGHT_BRACKET": ']',
    "RIGHT_PARAN": ')',
    "EQUAL": "="
}

TT_EOF = "EOF"
WHITESPACE_TAB_NEWLINE = {
    "WHITE_SPACE": ' ',
    "NEW_LINE": '\n',
    "TAB": '\t',
    "EOF": "EOF"
}

TT_LET = "LET"
KEYWORDS = {
    "LET": "let"
}

# create a method called is_keyword:







