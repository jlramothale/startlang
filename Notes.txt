Factor: is a single number: Int:Float
Term: are the numbers to be operated on: Factor */ Factor
Expr: is the whole statement: Term +- Term 


---------- Variables:

a variable is define with a let: expression
define a variable: let: a = 1
to access the variable we simple call the variable name: a

    let         :        variableName       =   <expr>
    ^           ^             ^   
KEYWORK     SEMICOLON      IDENTIFIER      EQUAL

keyword is the same as the identifier; the difference is that keyword is name built into the language

Does not allow for single line multiple declaration, i,e, let: a = 5, b = 5, c = 6