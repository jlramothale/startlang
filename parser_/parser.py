"""
    PARSER
"""
from parser_.nodes import (
    NumberNode, BinOpNode, UniryOpNode, VariableAssingNode, VariableAccessNode
)
from lexer_ import token as tt_tokens
from lib_.error import (
    InvalidSyntaxError
)
from lib_.results import ParserResult
from lib_.utils import TokenUtils


# the parser creates an InOrder Travesal Tree: Left to Right
class Parser:

    def __init__(self, tokens):
        self.tokens = tokens
        self.tokens_size = len(tokens)
        self.current_token = None
        self.current_index = -1
        self.nextToken()

    def nextToken(self):
        self.current_index += 1
        if self.current_index < self.tokens_size:
            self.current_token = self.tokens[self.current_index]
        # return self.current_token

    def parse(self):
        token_utils = TokenUtils()
        # if is variable declaration
        if token_utils.is_declaration_token(self.current_token):
            return self.parse_var_declaration()
        else:
            return self.parse_calculation()

    def parse_var_declaration(self):
        res = ParserResult()
        token_utils = TokenUtils()
        
        res.register_next()
        self.nextToken()
        if not token_utils.is_colon(self.current_token):
            return res.failure(InvalidSyntaxError(
                f"Expected colon '{tt_tokens.DELIMETERS.get('COLON')}' ", self.current_token.position_start, self.current_token.position_end
            ))
            
        res.register_next() 
        self.nextToken()
        if not token_utils.is_identifier(self.current_token):
            return res.failure(InvalidSyntaxError(
                "Expected identifier (variable name) ", self.current_token.position_start, self.current_token.position_end
            ))
        # if it is an identifier, assign varribale name to the current token
        variable_name = self.current_token
        
        res.register_next() 
        self.nextToken()
        if not token_utils.is_equals(self.current_token):
            return res.failure(InvalidSyntaxError(
                "Expected '=' ", self.current_token.position_start, self.current_token.position_end
            ))
        
        res.register_next()
        self.nextToken()
        expression = res.register(self.expr())
        
        if res.error: return res
        else:
            # creates a vriable with its value as the expression
            return res.success(VariableAssingNode(variable_name, expression))
                
    def parse_calculation(self):
        result = self.expr()
        if not result.error and self.current_token.type != tt_tokens.TT_EOF:
            return result.failure(InvalidSyntaxError(
            "Expected '+', '-', '*' or '/'", self.current_token.position_start, self.current_token.position_end
        ))
        return result
    
    def factor(self):
        res = ParserResult()
        token = self.current_token

        if token.type in (tt_tokens.TT_PLUS, tt_tokens.TT_MINUS):
            res.register_next() 
            self.nextToken()
            factor = res.register(self.factor())
            # if error, return the results
            if res.error: return res
            return res.success(UniryOpNode(token, factor))
        
        elif token.type in (tt_tokens.TT_INT, tt_tokens.TT_FLOAT):
            res.register_next() 
            self.nextToken()
            return res.success(NumberNode(token))
        
        elif token.type == tt_tokens.TT_IDENTIFIER:
            res.register_next() 
            self.nextToken()
            return res.success(VariableAccessNode(token))
        
        elif token.type == tt_tokens.TT_LEFT_PAREN:
            res.register_next() 
            self.nextToken()
            expr = res.register(self.expr())
            if res.error: return res
            if self.current_token.type == tt_tokens.TT_RIGHT_PAREN:
                res.register_next() 
                self.nextToken()
                return res.success(expr)
            else:
                return res.failure(InvalidSyntaxError(
                    "Expected ')'", self.current_token.position_start, self.current_token.position_end
                ))
                
        return res.failure(InvalidSyntaxError(
            "Expected int, float or identifier ", token.position_start, token.position_end
        ))

    def term(self):
        return self.bin_op(self.factor, (tt_tokens.TT_DIV, tt_tokens.TT_MUL))

    def expr(self):
        return self.bin_op(self.term, (tt_tokens.TT_PLUS, tt_tokens.TT_MINUS))

    def bin_op(self, func, OPS):
        res = ParserResult()
        # register takes the results and returns the node as the left factor, and als set error to the error returned from the binary func() call
        left_factor = res.register(func())

        # if error, return the results
        if res.error: return res

        while self.current_token.type in OPS:
            op_token = self.current_token
            res.register_next() 
            self.nextToken()
            right_factor = res.register(func())

            # if error, return the results
            if res.error: return res

            left_factor = BinOpNode(left_factor, op_token, right_factor)

        # if it is a success, return the successful node
        return res.success(left_factor)

def run(tokens):
    parser = Parser(tokens)
    ast = parser.parse()

    return ast.node, ast.error