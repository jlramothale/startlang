
"""
    NUMBER NODE: either an int or float number
"""
class NumberNode:

    def __init__(self, token):
        self.token = token # either integer or float token
        self.position_start = self.token.position_start
        self.position_end = self.token.position_end

    def __repr__(self):
        return f"{self.token}"

"""
    UNIRY OPERATION NODE: for uniry operations, i.e. -5. +5
"""
class UniryOpNode:

    def __init__(self, op_token, node):
        self.op_token = op_token
        self.node = node
        self.position_start = self.op_token.position_start
        self.position_end = self.node.position_end
        
    def __repr__(self):
        return f"({self.op_token}. {self.node})"

"""
    BINARY OPERATION NODE: either add, subtract, divide and multiply operatio
"""
class BinOpNode:

    def __init__(self, left_node, op_token, right_node):
        self.left_node = left_node
        self.op_token = op_token
        self.right_node = right_node
        
        self.position_start = self.left_node.position_start
        self.position_end = self.right_node.position_end

    def __repr__(self):
        return f"({self.left_node}, {self.op_token}, {self.right_node})"

"""
    DECLARATION NODE: variable declaration node
"""
class VariableAssingNode:
    
    def __init__(self, variable_token = None, value_node = None):
        self.variable_token = variable_token
        self.value_node = value_node
        self.position_start = self.variable_token.position_start
        self.position_end = self.value_node.position_end
        
    def __repr__(self):
        return f"({self.variable_token}, {self.value_node})"


class VariableAccessNode:
    
    def __init__(self, variable_token):
        self.variable_token = variable_token
        self.position_start = self.variable_token.position_start
        self.position_end = self.variable_token.position_end
    
    def __repr__(self):
        return f"({self.variable_token})"