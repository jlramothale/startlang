"""
    NUMBER: store numbers and operates on them with other numbers
"""

from lib_.error import RunTimeError

class Number:
    
    def __init__(self, value):
        self.value = value # the value of this number
        self.set_position() # init positions
        self.set_context() # init context
        
    # position is to keep track on the number in case of error; 
    def set_position(self, position_start=None, position_end=None):
        self.position_start = position_start
        self.position_end = position_end
        return self
    
    # sets the current context 
    def set_context(self, context=None):
        self.context = context
        return self

    # adds this number to the other number
    def add(self, other):
        if isinstance(other, Number):
            return Number(self.value + other.value).set_context(self.context), None
        
    # minus this number to the other number
    def minus(self, other):
        if isinstance(other, Number):
            return Number(self.value - other.value).set_context(self.context), None
        
    # mulitply this number to the other number
    def mulitply(self, other):
        if isinstance(other, Number):
            return Number(self.value * other.value).set_context(self.context), None
        
    # divide this number to the other number
    def divide(self, other):
        if isinstance(other, Number):
            if other.value == 0:
                return None, RunTimeError(
                    "Division by zero", 
                    other.position_start, 
                    other.position_end,
                    self.context # if there is an error, pass the context
                )
            
            return Number(self.value / other.value).set_context(self.context), None
        
    # make a shallow copy
    def copy(self):
        copy_number = Number(self.value)
        copy_number.set_position(self.position_start, self.position_end)
        copy_number.set_context(self.context)
        return copy_number
        
    # number representation method
    def __repr__(self):
        return str(self.value)

    
    
    