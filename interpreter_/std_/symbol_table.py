
"""
    SYMBOL TABLE: keeps track of all the variable names and their values
"""
class SymbolTable:
    
    def __init__(self):
        self.symbols = {}
        # parent symbole table; creates a new symbol table when a function is called
        # therefore, we must be able to access those symbols; and remove them once function call is done
        
        # The parent symbol table will have the global symbol table as its parent; the GST can be accessed 
        # anywhere in the program
        self.parent = None 
        
    # sets variable name with a value into the symbols dictionary
    def set(self, name, value):
        self.symbols[name] = value
      
    # gets a variable value using the name of the variable.
    # if the variable is not found, we check the parent if it has it
    def get(self, name):
        value = self.symbols.get(name, None)
        if value == None and self.parent:
            return self.parent.get(name)
        return value
    
    # deletes a variable from the symbols
    def remove(self, name):
        del self.symbols[name]
        