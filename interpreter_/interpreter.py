
"""
    INTERPRETER: this enterpretes and evaluate the final results
"""

from .std_.number import Number
from lexer_ import token as tt_tokens
from lib_.results import RuntimeResult
from lib_.error import RunTimeError, MSG
from .context import Context

class Interpreter:

    # this will process all the ndes and determine what to do
    def visit(self, node, context):
        # this automatically creates the visit method, based on the node type
        method_name = f"visit_{type(node).__name__}"
        # gets the actual method from the class; not visit method if no method is found
        method = getattr(self, method_name, self.no_visit_method)
        return method(node, context)

    def no_visit_method(self, node, context):
        res = RuntimeResult()
        return res.failure(RunTimeError(
            f"No visit_{type(node).__name__} method found",
        ))
    
    def visit_NumberNode(self, node, context):
        res = RuntimeResult()
        # a number is always successful
        return res.success(Number(node.token.value)
                           .set_context(context)
                           .set_position(node.position_start, node.position_end))

    # uniry operation evaluation
    def visit_UniryOpNode(self, node, context):
        res = RuntimeResult()
        number = res.register(self.visit(node.node, context))
        if res.error: return res
        
        error = None
        if node.op_token.type == tt_tokens.TT_MINUS:
            number, error = number.mulitply(Number(-1))
            
        if error:
            return res.failure(error)
        else:
            return res.success(number.set_position(node.position_start, node.position_end))
    
    # binary operation evaluation
    def visit_BinOpNode(self, node, context):
        res = RuntimeResult()
        # register left and right visit, and return any error
        left = res.register(self.visit(node.left_node, context))
        if res.error: return res
        
        right = res.register(self.visit(node.right_node, context))
        if res.error: return res
        
        result = None
        error = None
        if node.op_token.type == tt_tokens.TT_PLUS:
            result, error = left.add(right)
        elif node.op_token.type == tt_tokens.TT_MINUS:
            result, error = left.minus(right)
        elif node.op_token.type == tt_tokens.TT_MUL:
            result, error = left.mulitply(right)
        elif node.op_token.type == tt_tokens.TT_DIV:
            result, error = left.divide(right)
            
        if error:
            return res.failure(error)
        else:
            return res.success(result.set_position(node.position_start, node.position_end))
        
    def visit_VariableAssingNode(self, node, context):
        res = RuntimeResult()
        
        # this is the node to assign the variable name to
        variable = node.variable_token.value
        value = res.register(self.visit(node.value_node, context))
        if res.error: return res
        
        context.symbol_table.set(variable, value)
        return res.success(value)
    
    def visit_VariableAccessNode(self, node, context):
        res = RuntimeResult()
        
        variable = node.variable_token.value
        value = context.symbol_table.get(variable)
        if not value:
            return res.failure(RunTimeError(
                MSG.VARIABLE_NOT_DEFINED.format(variable),
                node.position_start, node.position_end,
                context
            ))
        
        # get the position of the variable value where it was access, incase of correct error reporting
        value = value.copy().set_position(node.position_start, node.position_end)
        return res.success(value)

from .std_.symbol_table import SymbolTable

global_symbol_table = SymbolTable()
# sets the default symbol to null = 0; i.e. null is equals to 0
global_symbol_table.set("null", Number(0)) 
def run(node):
    # create the root context, i will be updated along side the tress
    context = Context('<start>')
    context.symbol_table = global_symbol_table
    
    interpreter = Interpreter()
    result = interpreter.visit(node, context)
    
    return result.value, result.error