"""
    CONTEXT: used the track the current context of the program:
    i.e. the context could be a function, class, or the entire program
    This is useful to display the stack trace for the error
"""

class Context:
    
    def __init__(self, display_name, parent=None, parent_entry_position=None):
        self.display_name = display_name # diaply name of the conext
        self.parent = parent # parrent of the current context
        self.parent_entry_position = parent_entry_position # the entry position of the parent
        self.symbol_table = {} # keeps track of the context symbol table
        