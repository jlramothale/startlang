
"""
    Character Utils
"""
from lexer_.token import (
    DIGITS, LETTERS, DELIMETERS, ARIMETIC_OPERATORS, LITERALS, WHITESPACE_TAB_NEWLINE,
    KEYWORDS, INDETIFIER_CHARS
)

from lexer_ import token as tt_tokens

class CharUtils:

    def __init__(self): pass

    def is_digit(self, char):
        return char.isdigit()

    def is_letter(self, char):
        return char.isalpha()
    
    def is_identifier(self, char):
        return True if char in INDETIFIER_CHARS else False

    def is_dot(self, char):
        return True if char == DELIMETERS.get("DOT") else False
    
    def is_colon(self, char):
        return True if char == DELIMETERS.get("COLON") else False

    def is_newline(self, char):
        return True if char == WHITESPACE_TAB_NEWLINE.get("NEW_LINE") else False

    def is_whitespace_or_newline(self, char):
        return True if char in WHITESPACE_TAB_NEWLINE.values() else False

    def is_arithmatic_operator(self, char):
        return True if char in ARIMETIC_OPERATORS.values() else False

    def get_arithmatic_type(self, char):
        for key, value in ARIMETIC_OPERATORS.items():
            if char == value:
                return key
        return None
    
    def is_delimeter(self, char):
        return True if char in DELIMETERS.values() else False

    def get_delimeter_type(self, char):
        for key, value in DELIMETERS.items():
            if char == value:
                return key
        return None
    
    def is_keyword(self, word):
        return True if word in KEYWORDS.values() else False
    
    def get_keyword(self, char):
        for key, value in KEYWORDS.items():
            if char == value:
                return key
        return None

    def get_integer_type(self):
        return LITERALS.get("INTEGER")

    def get_float_type(self):
        return LITERALS.get("FLOAT")
    
    def get_keyword_type(self):
        return LITERALS.get("KEYWORD")
        
    def get_identifier_type(self):
        return LITERALS.get("IDENTIFIER")
                
class TokenUtils:
    
    def __init__(self):
        self.char_utils = CharUtils()
    
    def is_declaration_token(self, token):
        if token.type == self.char_utils.get_keyword_type() and token.value == KEYWORDS.get("LET"):
            return True
        return False
    
    def is_identifier(self, token):
        return True if token.type == self.char_utils.get_identifier_type() else False
    
    def is_colon(self, token):
        return True if token.type == tt_tokens.TT_COLON else False
    
    def is_equals(self, token):
        return True if token.type == tt_tokens.TT_EQUAL else False

class StringUtils:

    def __init__(self):
        pass

    # displays arrows where the errors came from
    def string_with_arrows(self, text, pos_start, pos_end):
        result = ''
        # Calculate indices
        idx_start = max(text.rfind('\n', 0, pos_start.index), 0)
        idx_end = text.find('\n', idx_start + 1)
        if idx_end < 0: idx_end = len(text)
        
        # Generate each line
        line_count = pos_end.line - pos_start.line + 1
        for i in range(line_count):
            # Calculate line columns
            line = text[idx_start:idx_end]
            col_start = pos_start.col if i == 0 else 0
            col_end = pos_end.col if i == line_count - 1 else len(line) - 1

            # Append to result
            result += line + '\n'
            result += ' ' * col_start + '^' * (col_end - col_start)

            # Re-calculate indices
            idx_start = idx_end
            idx_end = text.find('\n', idx_start + 1)
            if idx_end < 0: idx_end = len(text)

        return result.replace('\t', '')