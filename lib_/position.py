
"""
    POSITION
"""
from lib_.utils import CharUtils

class Position:

    def __init__(self, index, line, col, fname, ftext):
        self.index = index
        self.line = line
        self.col = col
        self.fname = fname
        self.ftext = ftext

    # moves to next index, updates line abd colomn
    def next(self, char=None):
        self.index += 1
        self.col += 1
        char_utils = CharUtils()
        if char == char_utils.is_newline(char):
            self.line += 1
            self.col = 0

        return self

    def copy(self):
        return Position(self.index, self.line, self.col, self.fname, self.ftext)
