"""
    ParserResult: holds the results of the parser
    Keeps track of the current parser results and error if any
"""
class ParserResult:

    def __init__(self):
        self.node = None
        self.error = None
        self.next_count = 0 # checks if we have moved to the next token

    def register_next(self):
        # this will simply register a move to the next token
        self.next_count += 1
        pass
        
    def register(self, results):
        # results are either a ParserResult or a Node
        # self.next_count += results.next_count
        if results.error: self.error = results.error
        return results.node

    def success(self, node):
        self.node = node # assign node and return self
        return self

    def failure(self, error):
        # if not self.error or self.next_count == 0:
        #     self.error = error # only assign error if we moved
        self.error = error 
        return self
    
"""
    RuntimeResult: holds the results of the intepreter
    Keeps track fs the current run-time results and error if any
"""
class RuntimeResult:
    
    def __init__(self):
        self.value = None # runtime results
        self.error = None
        
    def register(self, results):
        # if there is an error, store the error and return the value
        if results.error: self.error = results.error
        return results.value

    def success(self, value):
        self.value = value # update value
        return self

    def failure(self, error):
        self.error = error # update errir
        return self
    
    