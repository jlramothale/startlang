

"""
    ERROR: General error
"""
from lib_.utils import StringUtils

class Error:

    def __init__(self, error, message, position_start, position_end):
        self.error = error
        self.message = message
        self.position_start = position_start
        self.position_end = position_end

    def __str__(self):
        str_utils = StringUtils()
        result = "Error: {}, {}. File {}, line {}\n{}".format(
            self.error, self.message, self.position_start.fname, self.position_start.line + 1, 
            str_utils.string_with_arrows(self.position_start.ftext, self.position_start, self.position_end)
        )
        return result

"""
    IllegalCharError: error for the lexer process
"""
class IllegalCharError(Error):
    
    def __init__(self, message, position_start, position_end):
        super().__init__("Illegal Character", message, position_start, position_end)

"""
    InvalidSyntaxError: error for the parsing process
"""
class InvalidSyntaxError(Error):
    
    def __init__(self, message, position_start, position_end):
        super().__init__("Invalid Syntax", message, position_start, position_end)
        
"""
    RuntimeError: error for the interpreter process
"""
class RunTimeError(Error):
    
    def __init__(self, message, position_start, position_end, context):
        super().__init__("Runtime Error", message, position_start, position_end)
        self.context = context
    
    def generate_error_trace(self):
        result = ""
        position = self.position_start
        context = self.context
        
        while context:
            result = " >>> File {}, line {}, in {}\n".format(
                self.position_start.fname, 
                self.position_start.line + 1,
                context.display_name
            ) + result
            position = context.parent_entry_position
            context = context.parent
            
        return result
    
    def __str__(self):
        str_utils = StringUtils()
        result = "Error Stack Trace (most recent call last)\n{}Error: {}, {}.\n{}".format(
            self.generate_error_trace(),
            self.error, 
            self.message,
            str_utils.string_with_arrows(self.position_start.ftext, self.position_start, self.position_end)
        )
        return result

class MSG:
    
    VARIABLE_NOT_DEFINED = "'{}' is not defined"