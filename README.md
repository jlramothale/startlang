# Start Programing Langauge

The goal of Start is to allow programmers to write code using one of the 11 Official Languages in South Africa. Start will also support KiSwahili Language as a global African Propgramming Langauge.

# Getting Started 

## Hello Start!
print: "Hello Start!"

## Data Types
Start untyped, that is all data types are determined during runtime by the Intepreter.

## Comments
Single Line: A comment in starts with //. Anything from the // symbol to the end of line will be ignored by the Start interpreter.
``` javascript
// This is a comment

```

## Declaring variables
Variables are declared using a let keyword followed by a colomn.

``` javascript
let: a = 5 // declares an Integer variable a
let: str = "Start is cool" // declaires a String variable str
let: peter = Person() // declaires a Person object called petter
let: b = a, c = 5, d = "Hello Petter" // declaring multiple variables at once
```

## Display output
Any output or display is done with print expression

``` javascript
print: a // print a valiable
print: peter.name // print name of the object peter
print: a, "Hello Start!" // print more than one output; seperate each with a comma
```

## Conditions
Like in most programming languages, conditions are expressed using an if-else expression.
The condition must evaluate to a value of type Bool.

``` javascript
if: <condition> {
    // do something
} 
else: {
    // do something
}
```

The else block can be omitted if needed and the curly braces can be omitted for the if or else block if the block is made of only one expression.

``` javascript
if: a greater b {
    print: a
} 
else: {
    print: "b=", b, "; a=", a
}
```

## Looping
A while expression is used to execute one or more expressions as long as a condition holds true.

``` javascript
while: a greater b {
    // do something
}
```

Again, the curly braces can be omitted if the body of the while is made of only one expression.

``` javascript
while: a less 5 {
    print: a
    a++
}
```

## Defining functions
Functions are defined using the func keyword.
``` javascript
function: sum(a, b) {
    return a + b
}
```

### Function parameters
Are separated by commas , and enclosed in parenthesis (a, b). 
Each parameter is defined by its name followed.

### Return type. 
The return type value is determined by return expression value
If the function does not return a value, the return type will be void.

### Function body.
The body of the function is enclosed in curly braces. {}

``` javascript
function: sayHello() {
    print: "Hello Start World"
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Built with
- [Python](https://python.org)

## License
[MIT] © [StarX Technologies](http://star-x.co.za)

